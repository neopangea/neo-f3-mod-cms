/**
 * Created by Paul on 7/31/14.
 */

/*jslint nomen: true, white: true */
/*global angular, window, _*/

(function () {

    'use strict';

    angular.module('cmsApp').controller(
        'contentListCtrl',
        [
            '$scope',
            '$routeParams',
            'episodeFactory',
            'neoEnumHelper',
            function ($scope, $routeParams, episodeFactory, neoEnumHelper) {

                $scope.enumHelper = neoEnumHelper;
                $scope.ui    = {};
                $scope.data  = {
                    contents : []
                };

                $scope.state = {
                    options: ['default', 'fetching', 'error'],
                    current: 'default',
                    msg: '',
                    set: function (state, msg) {
                        if ($scope.state.options.indexOf(state) === -1) {
                            window.console.warn('unknown state: ', state);
                        }
                        $scope.state.msg = msg || '';
                        $scope.state.current = state;
                    }
                };

                /**
                 * Initialize
                 */

                $scope.state.set('fetching');

                episodeFactory.get({
                        eId: $routeParams.episode_eId,
                        action: 'contents'
                    },
                    function (response) {

                        if (!response.successful) {
                            $scope.state.set('error', response.message || 'check the js console for details');
                            window.console.warn('Error', response);
                            return;
                        }

                        $scope.state.set('default');

                        $scope.data.contents  = response.data.contents;
                        $scope.data.types     = response.data.types;
                        $scope.data.positions = response.data.positions;
                        $scope.data.episode   = response.data.episode;

                    }, function (error) {
                        $scope.state.set('error', error.status + '-' + error.statusText);
                        window.console.warn('Error:', error);
                    }
                );
            }
        ]
    );
}());

