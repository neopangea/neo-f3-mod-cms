/**
 * Created by Paul on 7/31/14.
 */

/*jslint nomen: true, white: true */
/*global angular, window, _, NP*/

(function () {

    'use strict';

    angular.module('cmsApp').controller(
        'contentEditCtrl',
        [
            '$scope',
            '$routeParams',
            'contentFactory',
            'neoEnumHelper',
            function ($scope, $routeParams, contentFactory, neoEnumHelper) {

                $scope.enumHelper = neoEnumHelper;
                $scope.npConfig = NP.config;

                $scope.data = {
                    episode: {},
                    content: null,
                    types: []
                };

                $scope.menu  = {
                    eId: null
                };

                $scope.state = {
                    options: ['default', 'fetching', 'saving', 'saved', 'error'],
                    current: 'default',
                    msg: '',
                    set: function (state, msg) {
                        if ($scope.state.options.indexOf(state) === -1) {
                            window.console.warn('unknown state: ', state);
                        }
                        $scope.state.msg = msg || '';
                        $scope.state.current = state;
                    }
                };

                $scope.save = function () {

                    $scope.state.set('saving');

                    contentFactory.save(
                        $scope.data.content,
                        function (response) {
                            if (response.successful) {
                                $scope.state.set('saved');
                            } else {
                                $scope.state.set('error', 'Please fix the errors below');
                                window.console.warn('Error: ', response);
                            }
                            $scope.data.content = response.data.content;
                        },
                        function (error) {
                            $scope.state.set('error', error.status + '-' + error.statusText);
                            window.console.warn('Error:', error);
                        }
                    );
                };

                /**
                 * Initialize
                 */

                //
                // New Content
                //

                if ($routeParams.episode_eId) {

                    $scope.state.set('fetching');

                    $routeParams.episode_eId = window.parseInt($routeParams.episode_eId);
                    contentFactory.get(
                        {},
                        function (response) {
                            if (!response.successful) {
                                $scope.state.set('error', response.message || 'Check JS console for details.');
                                window.console.warn('Error: ', response);
                                return;
                            }

                            $scope.handleInitResponse(response);

                            $scope.data.content = {
                                episodeEnumId: $routeParams.episode_eId,
                                typeEnumId: $scope.data.typeOptions[0].id,
                                positionEnumId: $scope.data.positionOptions[0].id
                            };

                            $scope.data.episode = _.findWhere(
                                $scope.data.episodeOptions,
                                {eId: $scope.data.content.episodeEnumId}
                            );

                            $scope.state.set('default');
                        }, function (error) {
                            $scope.state.set('error', error.status + '-' + error.statusText);
                            window.console.warn('Error:', error);
                        }
                    );
                }

                //
                // Edit Content
                //

                if ($routeParams.id) {

                    $scope.state.set('fetching');

                    contentFactory.get(
                        {id:$routeParams.id},
                        function (response) {
                            if (!response.successful) {
                                $scope.state.set('error', response.message || 'Check JS console for details.');
                                window.console.warn('Error: ', response);
                                return;
                            }

                            $scope.handleInitResponse(response);

                            $scope.data.content = response.data.content;

                            $scope.data.episode = _.findWhere(
                                $scope.data.episodeOptions,
                                {eId: $scope.data.content.episodeEnumId}
                            );

                            $scope.state.set('default');
                        }, function (error) {
                            $scope.state.set('error', error.status + '-' + error.statusText);
                            window.console.warn('Error:', error);
                        }
                    );
                }

                $scope.handleInitResponse = function (response) {
                    $scope.data.typeOptions     = response.data.typeOptions;
                    $scope.data.positionOptions = response.data.positionOptions;
                    $scope.data.episodeOptions  = response.data.episodeOptions;
                };
            }
        ]
    );
}());

