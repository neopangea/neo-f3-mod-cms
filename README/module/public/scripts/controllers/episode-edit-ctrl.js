/**
 * Created by Paul on 7/31/14.
 */

/*jslint nomen: true, white: true */
/*global angular, window, _*/

(function () {

    'use strict';

    angular.module('cmsApp').controller(
        'episodeEditCtrl',
        [
            '$scope',
            '$routeParams',
            'episodeFactory',
            'neoEnumHelper',
            function ($scope, $routeParams, episodeFactory, neoEnumHelper) {

                $scope.enumHelper = neoEnumHelper;

                $scope.data = {
                    episode: {}
                };

                $scope.state = {
                    options : { editing:'editing', fetching:'fetching', saving:'saving', saved:'saved', error:'error'},
                    current : 'editing',
                    msg: '',
                    set : function (state, msg) {
                        this.msg = msg || '';
                        this.current = state;
                    }
                };

                $scope.saveEpisode = function () {

                    $scope.state.set($scope.state.options.saving);

                    episodeFactory.save(
                        $scope.data.episode,
                        function (response) {
                            if (response.successful) {
                                $scope.state.set($scope.state.options.saved);
                            } else {
                                $scope.state.set($scope.state.options.error, 'Validation errors');
                            }
                            $scope.data.episode = response.data.episode;
                        },
                        function (err) {
                            $scope.state.set($scope.state.options.error);
                            $scope.stateMsg = err.statusText; //err.data;
                        }
                    );
                };

                /**
                 * Initialize
                 */

                if ($routeParams.eId) {
                    $scope.state.set($scope.state.options.fetching);
                    episodeFactory.get(
                        {eId: $routeParams.eId},
                        function (response) {
                            if (!response.successful) {
                                $scope.state.set('error');
                                $scope.state.msg = 'Server or Network error. Check JS console for details.';
                                window.console.warn('Error in response: ', response);
                            }
                            $scope.data.episode = response.data.episode;
                            $scope.state.set($scope.state.options.editing);
                        }, function (error) {
                            $scope.state.set($scope.state.options.error, error.status + '-' + error.statusText);
                            window.console.warn('Error:', error);
                        }
                    );
                }
            }
        ]
    );
}());

