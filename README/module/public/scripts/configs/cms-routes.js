/*jslint nomen: true, white: true */
/*global angular*/

(function () {

    'use strict';

    angular.module('cmsApp').config([
        '$routeProvider',
        function ($routeProvider) {
            $routeProvider
                .when('/episode/:eId', {
                    controller: 'episodeEditCtrl',
                    templateUrl: '/modules/cms/public/scripts/views/episode-edit-view.html'
                }).when('/episode/:episode_eId/contents', {
                    controller: 'contentListCtrl',
                    templateUrl: '/modules/cms/public/scripts/views/content-list-view.html'
                }).when('/episode/:episode_eId/contents/add', {
                    controller: 'contentEditCtrl',
                    templateUrl: '/modules/cms/public/scripts/views/content-edit-view.html'
                }).when('/content/edit/:id', {
                    controller: 'contentEditCtrl',
                    templateUrl: '/modules/cms/public/scripts/views/content-edit-view.html'
                }).otherwise({
                    redirectTo: '/episode/1'
                });
        }
    ]);
}());