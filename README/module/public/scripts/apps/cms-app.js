/*global angular*/

(function () {
    'use strict';

    angular.module('cmsApp', [
        'neoCmsBaseModule',
        'angular-redactor',
        'ngQuickDate',
        'filesApp',
        'ngCookies',
        'ngResource',
        'ngSanitize',
        'ngRoute'
    ]);

}());
