/**
 * Created by Paul on 7/31/14.
 */

/*jslint nomen: true, white: true */
/*global angular, NP*/

(function () {

    'use strict';

    angular.module('cmsApp').factory(
        'episodeFactory',
        [
            '$resource',
            function ($resource) {
                return $resource('/cms/services/episodes/:eId/:action', {action:'@action'});
            }
        ]
    );

}());