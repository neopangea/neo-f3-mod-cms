/**
 * Created by Paul on 7/31/14.
 */

/*jslint nomen: true, white: true */
/*global angular, NP*/

(function () {

    'use strict';

    angular.module('cmsApp').factory(
        'contentFactory',
        [
            '$resource',
            function ($resource) {
                return $resource('/cms/services/contents/:id');
            }
        ]
    );

}());

