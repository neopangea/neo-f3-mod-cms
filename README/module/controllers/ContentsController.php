<?php

namespace BG\Cms;

use \Neo\F3        as Neo;
use \Neo\Lib\Enums as Enums;
use \Neo\Lib\Utils as Utils;
use \BG\Models     as Models;

class ContentsController extends \Neo\F3\Controller {

    public function get($f3, $args) {

        \Neo\Cms\Lib\Auth::adminGate(\Neo\Cms\Lib\Auth::GATE_THROW_401);

        $response = new Neo\Response();
        $db       = Models\ContentDal::getDB($f3);

        $response->data->contentTypes = \BG\Lib\Enums\ContentType::getAll();

        if (isset($args['_id'])) {
            $id = $args['_id'];
            $mongoId = new \MongoId($id);
            $entity  = Models\ContentDal::get($db, $mongoId);
            $viewModel = new Models\ContentViewModel($entity);
            $response->data->content = $viewModel;
        }

        $response->data->episodeOptions = Models\EpisodeViewModel::getViewModels(
            Models\EpisodeDal::getAll($db)
        );

        $response->data->typeOptions     = \BG\Lib\Enums\ContentType::getAll();
        $response->data->positionOptions = \BG\Lib\Enums\ContentPosition::getAll();

        exit(json_encode($response));
    }

    public function post( $f3, $args ) {

        $response = new \Neo\F3\Response();
        $post     = json_decode($f3->get('BODY'));
        $db       = Models\ContentDal::getDB($f3);
        $entity   = new Models\ContentEntity();

        if (isset($post->_id)) {
            $entity = Models\ContentDal::get($db, $post->_id);
            if (is_null($entity)) { $f3->error('403', 'missing record');}
        }

        $mapper = new Models\ContentPostDataMapper();
        $mapper->map($entity, $post);

        if (!$entity->validate()) {
            $response->successful = false;
        } else {
            if (!Models\ContentDal::save($db, $entity)) { $f3->error('500', 'unable to save'); }
        }
        $response->data->content = new Models\ContentViewModel($entity);

        exit(json_encode($response));
    }
}