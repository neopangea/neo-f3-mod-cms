<?php

namespace BG\Cms;

use \Neo\F3        as Neo;
use \Neo\Lib\Enums as Enums;
use \Neo\Lib\Utils as Utils;
use \BG\Models     as Models;

class EpisodesController extends \Neo\F3\Controller {

    public function index( $f3, $args ) {
        parent::__construct( $f3 );

        \Neo\Cms\Lib\Auth::adminGate();
        $template = \Template::instance();
        echo $template->render('modules/cms/templates/index.htm');
    }

    public function get($f3, $args) {

        \Neo\Cms\Lib\Auth::adminGate(\Neo\Cms\Lib\Auth::GATE_THROW_401);

        $response = new Neo\Response();
        $db       = Models\EpisodeDal::getDB($f3);

        if (isset($args['eId'])) {
            $entity = Models\EpisodeDal::getByEid($db, (int)$args['eId']);
            // If entity not found, create seed entity
            if (!$entity) {$entity = array('eId' => $args['eId']);}
            $viewModel = new Models\EpisodeViewModel($entity);
            $response->data->episode = $viewModel;
        } else {
            $f3->error('500', 'get all episodes api not implemented');
            // Return All
            //$response->data->episodes = ALL
        }
        exit(json_encode($response));
    }

    public function post( $f3, $args ) {

        $response = new \Neo\F3\Response();
        $post     = json_decode($f3->get('BODY'));
        $db       = Models\EpisodeDal::getDB($f3);
        $entity   = new Models\EpisodeEntity();

        if (isset($post->_id)) {
            $entity = Models\EpisodeDal::get($db, $post->_id);
            if (is_null($entity)) { $f3->error('403', 'missing record');}
        }

        $mapper = new Models\EpisodePostDataMapper();
        $mapper->map($entity, $post);

        if (!$entity->validate()) {
            $response->successful = false;
        } else {
            if (!Models\EpisodeDal::save($db, $entity)) { $f3->error('500', 'unable to save'); }
        }
        $response->data->episode = new Models\EpisodeViewModel($entity);

        exit(json_encode($response));
    }

    public function contentIndex($f3, $args) {

        \Neo\Cms\Lib\Auth::adminGate(\Neo\Cms\Lib\Auth::GATE_THROW_401);

        if (!isset($args['eId'])) { $f3->error('404', 'missing variable: eId'); }

        $response = new Neo\Response();
        $db       = Models\ContentDal::getDB($f3);

        $episode = Models\EpisodeDal::getByEid($db, (int)$args['eId']);
        $response->data->episode = new Models\EpisodeViewModel($episode);

        $contents = Models\ContentDal::getByEpisodeEid($db, (int)$args['eId']);
        $response->data->contents = Models\ContentViewModel::getViewModels($contents);


        $response->data->types     = \BG\Lib\Enums\ContentType::getAll();
        $response->data->positions = \BG\Lib\Enums\ContentPosition::getAll();

        exit(json_encode($response));
    }
}