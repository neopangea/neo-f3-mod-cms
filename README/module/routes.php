<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 8/18/14
 * Time: 5:44 PM
 */

$app = Neo\F3\App::instance();

$app->f3->route( 'GET  /cms/episodes'                        , '\BG\Cms\EpisodesController->index' );
$app->f3->route( 'GET  /cms/services/episodes/@eId'          , '\BG\Cms\EpisodesController->get'   );
$app->f3->route( 'POST /cms/services/episodes'               , '\BG\Cms\EpisodesController->post'  );
$app->f3->route( 'GET  /cms/services/episodes/@eId/contents' , '\BG\Cms\EpisodesController->contentIndex' );


$app->f3->route( 'GET  /cms/services/contents'      , '\BG\Cms\ContentsController->get'  );
$app->f3->route( 'GET  /cms/services/contents/@_id'  , '\BG\Cms\ContentsController->get'  );
$app->f3->route( 'POST /cms/services/contents'      , '\BG\Cms\ContentsController->post' );

$app->f3->route( 'GET  /cms/services/episode-content/@episode_eId', '\BG\Cms\ContentsController->getEpisodeContent'  );

