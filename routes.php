<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 7/20/14
 * Time: 6:39 PM
 */

$app = Neo\F3\App::instance();

$app->f3->route( 'GET  /'.  $app->f3->get('NEO_CMS_SLUG')               , 'Neo\Cms\CmsController->index'  );
$app->f3->route( 'POST /'.  $app->f3->get('NEO_CMS_SLUG') .'/upload/file'   , 'Neo\Cms\CmsController->uploadFile' );
