<?php

/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 9/19/14
 * Time: 4:58 PM
 */

namespace Neo\Cms\Lib;

/**
 * Class AuthBehavior
 *
 * Extend this class and register w/ Auth class for concrete usage
 * @package Neo\Cms\Lib
 */

class AuthBehavior {

    public function adminGate($behavior = Auth::GATE_REDIRECT) {
        $this->throwNotImplemented();
    }

    public function userGate($behavior = Auth::GATE_REDIRECT) {
        $this->throwNotImplemented();
    }

    public function isLoggedIn() {
        $this->throwNotImplemented();
    }

    public function loginByEmailAndPassword ($email, $password) {
        $this->throwNotImplemented();
    }

    public function logout () {
        $this->throwNotImplemented();
    }

    public function encryptPassword ($password) {
        $this->throwNotImplemented();
    }

    protected function throwNotImplemented () {
        throw new \Exception('Not Implemented: Extend AuthBehavior and register w/ Auth static class');
    }

}