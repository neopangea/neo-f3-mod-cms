<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 8/7/14
 * Time: 3:36 PM
 */

namespace Neo\Cms\Lib;

/**
 * Class ValitronExtender
 *
 * Call ValitronExtender::init() to pull in custom validation rules.
 *
 * @package Neo\Cms\Lib
 */

class ValitronExtender {

    public static function init () {

        \Valitron\Validator::addRule('arrayMin', function($field, $value, array $params) {

            if (empty($value)) {
                return false;
            }

            if (count($value) < $params[0]) {
                return false;
            }

            return true;

        }, 'min length = %s');

    }

} 