<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 9/19/14
 * Time: 4:58 PM
 */

namespace Neo\Cms\Lib;

/**
 * Class Auth
 * For concrete implementation extend AuthBehavior and register w/ Auth::registerBehavior method
 * @package Neo\Cms\Lib
 */

class Auth {

    const
        GATE_REDIRECT  = 'GATE_REDIRECT',
        GATE_THROW_401 = 'GATE_THROW_401',
        GATE_BOOL      = 'GATE_BOOL';

    public static $authBehavior;

    public static function staticInit() {
        return static::registerBehavior(new AuthBehavior());
    }

    public static function registerBehavior($behavior) {
        return static::$authBehavior = $behavior;
    }

    public static function adminGate($behavior = Auth::GATE_REDIRECT) {
        return static::$authBehavior->adminGate($behavior);
    }

    public static function userGate($behavior = Auth::GATE_REDIRECT) {
        return static::$authBehavior->userGate($behavior);
    }

    public static function loginByEmailAndPassword($email, $password) {
        return static::$authBehavior->loginByEmailAndPassword($email, $password);
    }

    public static function logout() {
        return static::$authBehavior->logout();
    }

    public static function encryptPassword($password) {
        return static::$authBehavior->encryptPassword($password);
    }
}

Auth::staticInit();