# CMS MODULE - NEO F3 MOD CMS #

This CMS to be used w/ [NEO F3 APP Framework](https://bitbucket.org/neopangea/neo-f3-app)

# Installation #

1. Install [application framework](https://bitbucket.org/neopangea/neo-f3-app) 
3. Add a [composer dependency](https://packagist.org/packages/neopangea/f3-mod-cms) for this project to root project's composer.json
4. Run via command line 'composer update'
5. Copy entire contents of '/vendor/neopangea/f3-app/README/www' into web root.
6. Done!

# Usage #

Once installed navigate to /cms and basic CMS wrapper will be displayed.

# Modules #

* [File Manager](https://bitbucket.org/neopangea/neo-f3-mod-cms-files)

# Customizing #

By follow the below conventions, the CMS may be customized per application.

## Directory Structure ##

**Required - Custom CMS Module**

Create root directory...

```
   root/
      modules/
         cms-<module name>/
```

**Required - Configuration**

Add configuration files...

```
   root/
      modules/
         cms-<module name>/
            config/
               - common.ini
               - development.ini
               - production.ini
               - staging.ini
```

**Optional - CMS Main Nav Item**

Add Nav template file.

```
   /root
      /modules
         /cms-<module name>
            /views
               /nav
                  - main-nav.htm
``` 

Add Boostrap 3 formatted file item

```
<li class="<?= $nav['primary'] == 'files' ? 'active' : '' ?>">
    <a href="/cms/files"><span class="glyphicon glyphicon-file"></span> Files</a>
</li>
```

**Optional - Dictionary**

Add [F3 Dictionary](http://fatfreeframework.com/views-and-templates#multilingual-support)

```
/root
   /modules
      /cms-<module name>
         /dict
            - en.php
```            
           
- TODO: How to add scripts via head.thm via
- TODO: Add routes.php to module dir

The CMS manager w/ sniff these directories and pull in the required components.