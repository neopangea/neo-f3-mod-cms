<?php

namespace Neo\Cms;

use \Neo\Lib\Utils as Utils;

class CmsController extends \Neo\F3\Controller {

    public function index($f3, $args) {
        parent::__construct( $f3 );
        \Neo\Cms\Lib\Auth::adminGate(\Neo\Cms\Lib\Auth::GATE_REDIRECT);
        $template = \Template::instance();
        echo $template->render($f3->get('NEO_CMS_TMPL_PATH') .'cms/index.htm');
    }

    public function config ($f3, $args) {
        parent::__construct( $f3 );
        $view = \View::instance();
        echo $view->render($f3->get('NEO_CMS_VIEW_PATH') . 'config.htm');
    }

    public function uploadFile ($f3, $args) {
        parent::__construct( $f3 );

        \Neo\Cms\Lib\Auth::userGate(\Neo\Cms\Lib\Auth::GATE_THROW_401);

        if(!isset($_FILES['file'])) {
            throw new \Exception('Missing $_FILES["file"]');
        }

        $response = new \Neo\F3\Response();

        try {

            // Undefined | Multiple Files | $_FILES Corruption Attack
            // If this request falls under any of them, treat it invalid.
            if (
                !isset($_FILES['file']['error']) ||
                is_array($_FILES['file']['error'])
            ) {
                throw new \Exception('Invalid parameters.');
            }

            // Check $_FILES['uploadfile']['error'] value.
            switch ($_FILES['file']['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new \Exception('No file sent.');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new \Exception('Exceeded filesize limit.');
                default:
                    throw new \Exception('Unknown errors.');
            }

            $file_type   = Utils\File::getFileType($_FILES['file']['name']);
            $upload_path = Utils\File::trimRightSlash($f3->get('NEO_CMS_WYSIWYG_UPLOAD_PATH')) . "/{$file_type}";

            if (!move_uploaded_file(
                $_FILES['file']['tmp_name'],
                $upload_path . '/' . $_FILES['file']['name']
            )) {
                throw new \Exception('Failed to move uploaded file.');
            };

            // REDACTOR SPECIFIC RESPONSE
            $response->filelink = '/' . $upload_path . '/' . $_FILES['file']['name'];

        } catch (\Exception $e) {

            $response->successful = false;
            $response->message    = $e->getMessage();
        }

        exit(json_encode($response));
    }

}