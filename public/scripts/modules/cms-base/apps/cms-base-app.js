/**
 * Created by Paul Palladino on 8/5/14.
 */

/*global angular*/

(function () {
    'use strict';

    angular.module('neoCmsBaseModule', [
        'ngCookies',
        'ngResource',
        'ngSanitize',
        'ngRoute'
    ]).constant('config', {
        domain: NP.config.domain,
        cmsSlug: NP.config.cmsSlug
    });

}());
