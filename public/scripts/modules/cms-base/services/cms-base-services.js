/**
 * Created by Paul on 10/1/14.
 */

/*jslint nomen: true, white: true */
/*global angular, _, window, FormData */

(function () {

    'use strict';

    angular.module('neoCmsBaseModule').factory(
        'neoEnumHelper',
        [
            function () {
                return {
                    get: function (id, enums) {
                        id = window.parseInt(id);
                        var result =  _.findWhere(enums, {id: id});
                        return result || false;
                    },
                    getId: function (id, enums) {
                        id = window.parseInt(id);
                        var result =  _.findWhere(enums, {id: id});
                        return result ? result.id : false;
                    },
                    getLabel: function (id, enums) {
                        id = window.parseInt(id);
                        var result =  _.findWhere(enums, {id: id});
                        return result ? result.label : false;
                    },
                    getKey: function (id, enums) {
                        id = window.parseInt(id);
                        var result =  _.findWhere(enums, {id: id});
                        return result ? result.key : false;
                    },
                    getByKey: function (key, enums) {
                        var result =  _.findWhere(enums, {key: key});
                        return result || false;
                    },
                    getIdByKey: function (key, enums) {
                        var result =  _.findWhere(enums, {key: key});
                        return result ? result.id : false;
                    },
                    getLabelByKey: function (key, enums) {
                        var result =  _.findWhere(enums, {key: key});
                        return result ? result.label : false;
                    }
                };
            }
        ]
    );

    angular.module('neoCmsBaseModule').service(
        'neoSimpleFileUpload',
        [
            '$http',
            function ($http) {
                this.uploadFileToUrl = function(file, uploadUrl){
                    var fd = new FormData();
                    fd.append('file', file);
                    $http.post(uploadUrl, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    })
                    .success(function(){
                    })
                    .error(function(){
                    });
                };
            }
        ]
    );
}());
