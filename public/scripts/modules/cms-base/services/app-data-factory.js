/*jslint nomen: true, white: true, unused: false */
/*global angular*/

(function() {

    'use strict';

    angular.module('neoCmsBaseModule').factory(
        'appDataFactory',
        [
            '$resource',
            'config',
            function($resource, config) {

                return $resource('http://' + config.domain + '/api');

            }
        ]
    );

}());
