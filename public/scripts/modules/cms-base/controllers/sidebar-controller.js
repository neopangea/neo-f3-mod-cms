/*jslint nomen: true, white: true */
/*global angular*/

(function () {

    'use strict';

    angular.module('neoCmsBaseModule').controller(
        'sidebarController',
        [
            '$rootScope',
            '$log',
            '$location',
            '$scope',
            '$routeParams',
            'config',
            function ($rootScope, $log, $location, $scope, $routeParams, config) {

                $scope.ui       = {};
                $scope.data     = {};
                $scope.config   = config;
                $scope.uiReady = true;

                var href = document.location.href;

                $scope.isAdmin = false;
                if(NP.config.role == 'admin'){
                    $scope.isAdmin = true;
                }

                if(href.indexOf('login') > -1){
                    $scope.uiReady = false;
                } else {
                    $scope.uiReady = true;
                }

            }
        ]
    );

}());