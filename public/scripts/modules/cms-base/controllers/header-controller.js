/*jslint nomen: true, white: true */
/*global angular*/

(function () {

    'use strict';

    angular.module('neoCmsBaseModule').controller(
        'headerController',
        [
            '$rootScope',
            '$log',
            '$location',
            '$scope',
            '$routeParams',
            'config',
            function ($rootScope, $log, $location, $scope, $routeParams, config) {

                $scope.ui       = {};
                $scope.data     = {};
                $scope.config   = config;
                $scope.uiReady = true;

                var href = document.location.href;
                var data = null;

                $scope.isAdmin = false;
                if(NP.config.role == 'admin'){
                    $scope.isAdmin = true;
                }

                $rootScope.$on('dataReady', function(){
                    if(typeof $rootScope.data != 'undefined'){
                        $scope.data = $rootScope.data;
                        $scope.uiReady = true;
                        if(href.indexOf('login') > -1){
                            $scope.uiReady = false;
                        }
                    }
                });


                $scope.$watch(function() {
                    return $rootScope.data;
                }, function() {
                    $scope.data = $rootScope.data;
                }, true);

                /* INIT */


                console.debug('headerController', $scope.data);


            }
        ]
    );

}());