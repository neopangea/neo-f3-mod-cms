/*jslint nomen: true, white: true */
/*global angular, confirm, console, _, alert, window, $*/

(function () {
    'use strict';

    /**
     * A generic confirmation for risky actions.
     * Usage: Add attributes: neo-really-message="Are you sure"? neo-really-click="takeAction()"
     * neo-prevent-default="false" is optional
     */

    angular.module('neoCmsBaseModule').directive('neoReallyClick', [
        function () {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    element.bind('click', function () {

                        var message = attrs.neoReallyMessage;
                        if (message && confirm(message)) {
                            scope.$apply(attrs.neoReallyClick);
                        }

                        if (attrs.neoPreventDefault !== "false") {
                            return false;
                        }
                    });
                }
            };
        }
    ]);

    /**
     * Wraps Bootsrap popover. Set attributes for: title & data-content
     * Example: <neo-pop-over title="Foo" data-content="Bar"></neo-pop-over>
     */

    angular.module('neoCmsBaseModule').directive('neoPopOver', [
        function () {
            return {
                restrict: 'E',
                scope: {},
                replace: true,
                link: function (scope, element, attrs) {
                    $(element).popover({container: 'body'});
                },
                templateUrl: '/vendor/neopangea/f3-mod-cms/public/scripts/modules/cms-base/partials/popover-partial.html'
            };
        }
    ]);

    /**
     * Gets label of a neo php enum. Pass enum array into data-neo-enum-options attribute, and the enum of interest's
     * id into data-neo-enum-id attribute. If found, the enum's label will replace element's html
     */

    angular.module('neoCmsBaseModule').directive('neoEnumLabel', [
        function () {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    attrs.neoEnumOptions = JSON.parse(attrs.neoEnumOptions);
                    var enumItem = _.findWhere(attrs.neoEnumOptions, {id: window.parseInt(attrs.neoEnumId)});
                    if (enumItem) {
                        element.html(enumItem.label);
                    }
                }
            };
        }
    ]);

    /**
     * For simple file uploads, applies file information to scope variable handed off as directive attribute value
     * EX: <input neo-simple-file-model="data.upload.file" type="file" id="upload-file" required>
     * Maybe used w/ cms base service: neoSimpleFileUpload
     * EX: neoSimpleFileUpload.uploadFileToUrl($scope.data.upload.file, "/fileUpload");
     */

    angular.module('neoCmsBaseModule').directive('neoSimpleFileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var model = $parse(attrs.neoSimpleFileModel),
                    modelSetter = model.assign;

                element.bind('change', function(){
                    scope.$apply(function(){
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }]);


}());

